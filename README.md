## Font Custom

**Icon fonts from the command line.**

Generate cross-browser icon fonts and supporting files (@font-face CSS, etc.)
from a collection of SVGs 
([example](https://rawgit.com/FontCustom/fontcustom/master/spec/fixtures/example/example-preview.html)).

[Bugs/Support](https://github.com/FontCustom/fontcustom)

### Installation

Requires **Ruby 1.9.2+**, **FontForge** with Python scripting.

```sh
# On Mac
brew install fontforge --with-python
brew install eot-utils
gem install fontcustom

# On Linux
sudo apt-get install fontforge
wget http://people.mozilla.com/~jkew/woff/woff-code-latest.zip
unzip woff-code-latest.zip -d sfnt2woff && cd sfnt2woff && make && sudo mv sfnt2woff /usr/local/bin/
gem install fontcustom
```

### Quick Start

1. Place all SVG files into 'svg' folder.
2. Compile using:

```sh
fontcustom compile my/vectors  # Compiles icons into `fontcustom/`
fontcustom watch my/vectors    # Compiles when vectors are changed/added/removed
fontcustom compile             # Uses options from `./fontcustom.yml` or `config/fontcustom.yml`
fontcustom config              # Generate a blank a config file
fontcustom help                # See all options
```

### SVG Guidelines

* All colors will be rendered identically. Watch out for white fills!
* Remember about proper naming, e.g. for class 'icon-close' svg file should name 'close.svg'
* Use only solid colors. SVGs with transparency will be skipped.
* For greater precision in curved icons, use fills instead strokes and [try
  these solutions](https://github.com/FontCustom/fontcustom/issues/85).
* Activating `autowidth` trims horizontal white space from each glyph. This
  can be much easier than centering dozens of SVGs by hand.